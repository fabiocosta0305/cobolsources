       IDENTIFICATION DIVISION.
       PROGRAM-ID. TESTCOB.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 GROUP-FLD.
           05 FLD1        PIC X(4).
           05 FLD2        PIC X(6).
       77 ALPHA-FLD        PIC X(8).
       77 NUM-FLD1        PIC 9(4)V9.
       77 NUM-FLD2        PIC S9(3)V99.

       PROCEDURE DIVISION.

           MOVE 'ABCDEF' TO GROUP-FLD, ALPHA-FLD.

           IF GROUP-FLD = ALPHA-FLD THEN
              DISPLAY 'Alpha-Fld is equal group'
           END-IF.

           IF GROUP-FLD = FLD2 THEN
              DISPLAY 'FLD2 IS EQUAL GROUP-FLD'
           ELSE
              DISPLAY 'FLD2 IS ' FLD2
           END-IF.

           MOVE 7.1 TO NUM-FLD1, NUM-FLD2.

           IF NUM-FLD1 = NUM-FLD2
           THEN
              DISPLAY 'Number fields are equal'
           ELSE
              DISPLAY 'Number fields are not equal'
           END-IF.

           GOBACK.
