       IDENTIFICATION DIVISION.
       PROGRAM-ID. FINANCE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 SERIES-AMT1               PIC 9(9)V99     VALUE 100.
       01 SERIES-AMT2               PIC 9(9)V99     VALUE 200.
       01 SERIES-AMT3               PIC 9(9)V99     VALUE 300.
       01 DISCOUNT-RATE             PIC S9(2)V9(6)  VALUE .10.
       01 TODAYS-VALUE              PIC 9(9)V99.
       PROCEDURE DIVISION.
           COMPUTE TODAYS-VALUE =
              FUNCTION
                 PRESENT-VALUE(DISCOUNT-RATE SERIES-AMT1 SERIES-AMT2
                 SERIES-AMT3).
           DISPLAY "Payment #1 = " SERIES-AMT1.
           DISPLAY "Payment #2 = " SERIES-AMT2.
           DISPLAY "Payment #3 = " SERIES-AMT3.
           DISPLAY "Discount Rate = " DISCOUNT-RATE.
           DISPLAY "Today  Value = " TODAYS-VALUE.
           GOBACK.
