       01 CLAIM-PAGE-HDR.
           05 CLAIM-DATE-HDR.
              10 CLAIM-YEAR-HDR       PIC 9(4)    VALUE ZERO.
              10 FILLER               PIC X       VALUE '/'.
              10 CLAIM-MONTH-HDR      PIC 9(2)    VALUE ZERO.
              10 FILLER               PIC X       VALUE '/'.
              10 CLAIM-DAY-HDR        PIC 9(2)    VALUE ZERO.
           05 FILLER                  PIC X(46)   VALUE SPACES.
           05 FILLER                  PIC X(25)
              VALUE 'GROUP CLAIMS DAILY TOTALS'.
           05 FILLER                  PIC X(51)   VALUE SPACES.

       01 CLAIM-HDR-1.
           05 FILLER   PIC X(24) VALUE 'POLICY                  '.
           05 FILLER   PIC X(11) VALUE 'POLICY     '.
           05 FILLER   PIC X(15) VALUE 'FIRST          '.
           05 FILLER   PIC X(12) VALUE 'LAST        '.
           05 FILLER   PIC X(16) VALUE 'RENEW           '.
           05 FILLER   PIC X(6)  VALUE 'COPAY '.
           05 FILLER   PIC X(8)  VALUE ' COPAY  '.
           05 FILLER   PIC X(10) VALUE 'DEDUC     '.
           05 FILLER   PIC X(15) VALUE 'CLAIM          '.
           05 FILLER   PIC X(10) VALUE 'CLAIM     '.
           05 FILLER   PIC X(5)  VALUE SPACES.

       01 CLAIM-HDR-2.
           05 FILLER   PIC X(24) VALUE 'TYPE                    '.
           05 FILLER   PIC X(11) VALUE 'NUMBER     '.
           05 FILLER   PIC X(15) VALUE 'NAME           '.
           05 FILLER   PIC X(12) VALUE 'NAME        '.
           05 FILLER   PIC X(16) VALUE 'DATE            '.
           05 FILLER   PIC X(6)  VALUE ' MET  '.
           05 FILLER   PIC X(8)  VALUE 'PERCENT'.
           05 FILLER   PIC X(10) VALUE 'AMOUNT    '.
           05 FILLER   PIC X(15) VALUE 'AMOUNT         '.
           05 FILLER   PIC X(10) VALUE 'PAID      '.
           05 FILLER   PIC X(5)  VALUE SPACES.


       01 CLAIM-LINE-1.
           05 FILLER   PIC X(23) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(10) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(14) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(11) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(15) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(5)  VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(7)  VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(9)  VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(14) VALUE ALL '-'.
           05 FILLER   PIC X     VALUE SPACE.
           05 FILLER   PIC X(10)  VALUE ALL '-'.
           05 FILLER   PIC X(6)  VALUE SPACE.


       01 CLAIM-DATA-RPT.
           05 POLICY-TYPE-RPT         PIC X(23) VALUE SPACES.
           05 FILLER                  PIC X     VALUE SPACE.
           05 POLICY-NUMBER-RPT       PIC X(10) VALUE SPACES.
           05 FILLER                  PIC X     VALUE SPACE.
           05 POLICY-FIRST-NAME-RPT   PIC X(14) VALUE SPACE.
           05 FILLER                  PIC X     VALUE SPACE.
           05 POLICY-LAST-NAME-RPT    PIC X(11) VALUE SPACE.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-DATE-RPT.
              10 FILLER               PIC XXX       VALUE SPACES.
              10 POLICY-YEAR-RPT      PIC 9(4).
              10 FILLER               PIC X    VALUE "/".
              10 POLICY-MONTH-RPT     PIC 9(2).
              10 FILLER               PIC X    VALUE "/".
              10 POLICY-DAY-RPT       PIC 9(2).
              10 FILLER               PIC XX       VALUE SPACES.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-COPAY-RPT.
              10 FILLER                  PIC X(2) VALUE SPACES.
              10 POLICY-COPAY-MET-RPT    PIC X VALUE SPACES.
              10 FILLER                  PIC X(2) VALUE SPACES.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-PCT-RPT.
              10 FILLER   PIC X  VALUE SPACE.
              10 POLICY-COPAY-PCT-RPT    PIC .9(3).
              10 FILLER   PIC XX  VALUE SPACE.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-DEDUCT-RPT.
              10 FILLER   PIC XX VALUE SPACE.
              10 POLICY-DEDUCT-PAID-RPT PIC $9(4).
              10 FILLER   PIC XX VALUE SPACE.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-AMT-RPT.
              10 FILLER PIC X VALUE SPACE.
              10 POLICY-CLAIM-AMT-RPT PIC $9,999,999.99 VALUE ZERO.
              10 FILLER PIC X VALUE SPACE.
           05 FILLER   PIC X     VALUE SPACE.
           05 POLICY-PAID-RPT.
      *        10 FILLER PIC X VALUE SPACE.
              10 POLICY-CLAIM-PAID-RPT PIC $9,999.99 VALUE ZERO.
      *        10 FILLER PIC X VALUE SPACE.
           05 FILLER   PIC X(6)  VALUE SPACE.

