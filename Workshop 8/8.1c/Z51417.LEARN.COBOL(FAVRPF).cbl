       IDENTIFICATION DIVISION.
       PROGRAM-ID. FAVRPT.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT FAV-IN ASSIGN TO RPFIN.
           SELECT FAV-OUT ASSIGN TO PROPOSAL.

       DATA DIVISION.
       FILE SECTION.
       FD FAV-IN
           RECORDING MODE IS F
           DATA RECORD IS RFP-REC.
       01 RFP-REC.
           05 ARTIST-ACCT-NO                            PIC X(08).
           05 ARTIST-MUSICAL-GENRE                      PIC X(06).
              88 ROCK              VALUE 'ROCK'.
              88 JAZZ              VALUE 'JAZZ'.
              88 FUSION            VALUE 'FUSION'.
           05 MUSICIAN.
              10 MUSICIAN-LNAME                         PIC X(15).
              10 MUSICIAN-FNAME                         PIC X(15).
           05 MUSICIAN-INSTRUMENT-TYPE                  PIC X(06).
           05 INSTRUMENT-QUALITY                        PIC X(01).
              88 USED-FLAG         VALUE 'U'.
              88 NEW-FLAG          VALUE 'N'.
              88 PREMIUM-FLAG      VALUE 'P'.
           05 MAX-MUSICIAN-BUDGET-AMOUNT                PIC 9(5)V99.
           05 SHIP-TO                                   PIC X(3).
              88 IN-COUNTRY        VALUE 'IN'.
              88 OUT-COUNTRY       VALUE 'OUT'.
           05 FILLER                                    PIC X(19).
       FD FAV-OUT.
       01 PRN-DATA                                      PIC X(80).

       WORKING-STORAGE SECTION.
       01 PROP-REC.
           05 ARTIST-ACCT-NO-O                             PIC X(08).
           05 ARTIST-MUSICAL-GENRE-O                       PIC X(06).
              88 ROCK              VALUE 'ROCK'.
              88 JAZZ              VALUE 'JAZZ'.
              88 FUSION            VALUE 'FUSION'.
           05 MUSICIAN-O.
              10 MUSICIAN-LNAME-O                          PIC X(15).
              10 MUSICIAN-FNAME-O                          PIC X(15).
           05 MUSICIAN-INSTRUMENT-TYPE-O                   PIC X(6).
              88 KEYBOARD          VALUE 'KEYS'.
              88 VOCALS            VALUE 'VOCALS'.
              88 GUITAR            VALUE 'GUITAR'.
              88 BASS              VALUE 'BASS'.
              88 DRUMS             VALUE 'DRUMS'.
              88 PERCUSSION        VALUE 'PERC'.
           05 INSTRUMENT-QUALITY-O                         PIC X(1).
              88 USED-FLAG-O       VALUE 'U'.
              88 NEW-FLAG-O        VALUE 'N'.
              88 PREMIUM-FLAG-O    VALUE 'P'.
           05 SHIP-TO-O                                    PIC X(3).
              88 IN-COUNTRY-O      VALUE 'IN'.
              88 OUT-COUNTRY-O     VALUE 'OUT'.
           05 COST-PER-INSTRUMENT-O                        PIC S9(7)V99.
           05 ADDITIONAL-COSTS-O.
              10 SHIPPING-COST-O                           PIC S9(4)V99.
              10 TAX-O                                     PIC S9(3)V99.
       01 FLAGS.
           05 LASTREC                                      PIC X.
           05 VALID-DATA                                   PIC X.
              88 IS-VALID-DATA     VALUE 'Y'.
              88 NOT-VALID-DATA    VALUE 'N'.
           05 ERROR-MESSAGE.
              10 REASON                                    PIC X(30).
              10 WRONG-DATA                                PIC X(50).
      *     05 QUALITY-EXTRA-VALUE                          PIC S9(4)V99.
      *01 MEMVARS.

       01 FORM.
           02 LINE1.
              03 FILLER          PIC X(10)  VALUE 'Musician: '.
              03 FIRST-NAME      PIC X(15)  VALUE SPACES.
              03 FILLER          PIC X(1)   VALUE ' '.
              03 LAST-NAME       PIC X(15)  VALUE SPACES.
              03 FILLER          PIC X(10)  VALUE SPACES.
              03 FILLER          PIC X(13)  VALUE 'Music Style: '.
              03 MUSIC-STYLE     PIC X(6)   VALUE SPACES.
              03 FILLER          PIC X(12)  VALUE SPACES.
           02 LINE2.
              03 FILLER          PIC X(12)  VALUE 'Instrument: '.
              03 INSTRUMENT-NAME PIC X(13)  VALUE SPACES.
              03 FILLER           PIC X(1)  VALUE '('.
              03 INSTRUMENT-STYLE PIC X(7)  VALUE SPACES.
              03 FILLER           PIC X(2)  VALUE ') '.
              03 FILLER           PIC X(10) VALUE SPACES.
              03 FILLER           PIC X(24)
                   VALUE 'Overseas Shipping (Cost '.
              03 SHIPPING-COST    PIC $ZZ,ZZ9.99 VALUE ZERO.
              03 FILLER           PIC X(6)     VALUE ')    '.
           02 LINE3.
              03 FILLER            PIC X(6)         VALUE 'Cost: '.
              03 TOTAL-COST        PIC $Z,ZZZ,ZZ9.99 VALUE ZERO.
              03 FILLER            PIC X(9)         VALUE ' (Taxes: '.
              03 TAXES             PIC $ZZ9.99 VALUE ZERO.
              03 FILLER            PIC X(1)         VALUE ')'.
              03 FILLER            PIC X(44) VALUE SPACES.
           02 LINE4.
              03 FILLER            PIC X(80) VALUE ALL '-'.

       PROCEDURE DIVISION.

       DAPROGRAM.

           PERFORM START-FILES.
           PERFORM PROCESS-RECORDS.
           PERFORM CLOSE-FILES.
           GOBACK.

       START-FILES.
           OPEN INPUT FAV-IN.
           OPEN OUTPUT FAV-OUT.

       CLOSE-FILES.
           CLOSE FAV-IN.
           CLOSE FAV-OUT.

       PROCESS-RECORDS.
           PERFORM READ-NEXT-RECORD
              PERFORM UNTIL LASTREC = 'Y'
              PERFORM VALIDATE-DATA
              PERFORM PROCESS-DATA
              PERFORM READ-NEXT-RECORD
              END-PERFORM.

       READ-NEXT-RECORD.
           READ FAV-IN
              AT END MOVE 'Y' TO LASTREC
              END-READ.

       PROCESS-DATA.
           PERFORM VALIDATE-DATA.
           IF IS-VALID-DATA THEN PERFORM CALCULATE-VALUES END-IF.
           IF IS-VALID-DATA THEN PERFORM WRITE-DATA.
           IF NOT-VALID-DATA THEN PERFORM DISPLAY-ERROR-MESSAGE END-IF.

       VALIDATE-DATA.
           MOVE 'Y' TO VALID-DATA.

           IF ARTIST-ACCT-NO IS NOT NUMERIC THEN
              MOVE 'N' TO VALID-DATA
              MOVE 'NO NUMERIC ARTIST' TO REASON
              MOVE ARTIST-ACCT-NO TO WRONG-DATA
           END-IF.

           IF MAX-MUSICIAN-BUDGET-AMOUNT IS NOT NUMERIC THEN
              MOVE 'N' TO VALID-DATA
              MOVE 'NO NUMERIC BUDGET' TO REASON
              MOVE SPACES TO WRONG-DATA
           END-IF.

       CALCULATE-VALUES.
           PERFORM MOVE-DATA.
           IF IS-VALID-DATA THEN
              ADD SHIPPING-COST-O TO COST-PER-INSTRUMENT-O
              COMPUTE TAX-O = COST-PER-INSTRUMENT-O * 0.08
           END-IF.

       MOVE-DATA.
           MOVE ARTIST-ACCT-NO TO ARTIST-ACCT-NO-O.
           MOVE ARTIST-MUSICAL-GENRE TO ARTIST-ACCT-NO-O.
           MOVE MUSICIAN TO MUSICIAN-O.
           MOVE MUSICIAN-INSTRUMENT-TYPE
              TO MUSICIAN-INSTRUMENT-TYPE-O.
           MOVE INSTRUMENT-QUALITY TO INSTRUMENT-QUALITY-O.
           MOVE SHIP-TO TO SHIP-TO-O.

           EVALUATE TRUE
              WHEN KEYBOARD MOVE 3017.89 TO COST-PER-INSTRUMENT-O
              WHEN VOCALS MOVE 599.05 TO COST-PER-INSTRUMENT-O
              WHEN GUITAR MOVE 2648.99 TO COST-PER-INSTRUMENT-O
              WHEN BASS MOVE 18761 TO COST-PER-INSTRUMENT-O
              WHEN DRUMS MOVE 3087.22 TO COST-PER-INSTRUMENT-O
              WHEN PERCUSSION MOVE 799.99 TO COST-PER-INSTRUMENT-O
              WHEN OTHER PERFORM DEFAULT-ON-ERROR
           END-EVALUATE.

           EVALUATE TRUE
              WHEN USED-FLAG-O
                 COMPUTE COST-PER-INSTRUMENT-O =
                    COST-PER-INSTRUMENT-O -
                    (COST-PER-INSTRUMENT-O * 0.2)
              WHEN PREMIUM-FLAG-O
                 COMPUTE COST-PER-INSTRUMENT-O =
                    COST-PER-INSTRUMENT-O * 1.2
              WHEN NEW-FLAG-O
                 CONTINUE
              WHEN OTHER PERFORM DEFAULT-ON-ERROR
           END-EVALUATE.

           EVALUATE TRUE
              WHEN IN-COUNTRY-O
                 COMPUTE SHIPPING-COST-O = COST-PER-INSTRUMENT-O * 0.1
              WHEN OUT-COUNTRY-O
                 COMPUTE SHIPPING-COST-O = COST-PER-INSTRUMENT-O * 0.2
              WHEN OTHER PERFORM DEFAULT-ON-ERROR
           END-EVALUATE.

       DEFAULT-ON-ERROR.
           MOVE 0 TO COST-PER-INSTRUMENT-O.
           MOVE 'PROBLEM WITH THE DATA' TO REASON.
           MOVE SPACES TO WRONG-DATA.
           MOVE 'N' TO VALID-DATA.

       DISPLAY-ERROR-MESSAGE.
           DISPLAY 'INVALID DATA:'.
           DISPLAY ERROR-MESSAGE.
           DISPLAY RFP-REC.

       WRITE-DATA.
           INITIALIZE FORM.

           MOVE MUSICIAN-FNAME-O TO FIRST-NAME.
           MOVE MUSICIAN-LNAME-O TO LAST-NAME.
           MOVE ARTIST-MUSICAL-GENRE TO MUSIC-STYLE.

           MOVE LINE1 TO PRN-DATA.
           WRITE PRN-DATA.

           EVALUATE TRUE
              WHEN KEYBOARD   MOVE 'Keyboard'   TO INSTRUMENT-NAME
              WHEN VOCALS     MOVE 'Vocals'     TO INSTRUMENT-NAME
              WHEN GUITAR     MOVE 'Guitar'     TO INSTRUMENT-NAME
              WHEN BASS       MOVE 'Bass'       TO INSTRUMENT-NAME
              WHEN DRUMS      MOVE 'Drums'      TO INSTRUMENT-NAME
              WHEN PERCUSSION MOVE 'Percussion' TO INSTRUMENT-NAME
           END-EVALUATE.

           EVALUATE TRUE
              WHEN PREMIUM-FLAG-O MOVE 'Premium' TO INSTRUMENT-STYLE
              WHEN USED-FLAG-O    MOVE 'Used'    TO INSTRUMENT-STYLE
              WHEN NEW-FLAG-O     MOVE 'New'     TO INSTRUMENT-STYLE
           END-EVALUATE.

           MOVE SHIPPING-COST-O  TO SHIPPING-COST.

           MOVE LINE2 TO PRN-DATA.
           WRITE PRN-DATA.

           MOVE COST-PER-INSTRUMENT-O TO TOTAL-COST.
           MOVE TAX-O  TO TAXES.

           MOVE LINE3 TO PRN-DATA.
           WRITE PRN-DATA.

           MOVE LINE4 TO PRN-DATA.
           WRITE PRN-DATA.
