## Checkpoint 1

+ How many variables are declared in total?

    12

+ How many digits are to the left and right of the implied decimal place in SPECIAL-CHARGES?

    Left:    7
    Right:   3

+ Why is HOSP-BED declared in the A Margin?
  
    Level Number 01 
 
+ Why is MED-NAME declared in the B Margin?

    Part of a Group Variable

+ How many Group fields are declared

9

+ What is the data type of MED-PRICE?

Signed Decimal

+ What is the max algebraic value of COST?

999999

+ What is the total length of the MEDICATION group field?

59

+ How many signed numeric fields are there in HOSP-BED?

2

+ How many bytes can REC-KTR contain?

2

+ How many bytes can MED-NAME contain?

40

+ What is the difference between the PIC clauses of MED-PRICE and SPECIAL-CHARGES?

Only the way they are written

## Checkpoint 2

+ Note that you will receive a 04 return code...why?


 ==000042==> IGYPA3087-W "INT-RATE (NUMERIC NON-INTEGER)" and "PRINCIPAL (NUMERIC INTEGER)" had no
                         digit positions in common.  The receiver will be set to zero at execution
                         time.
