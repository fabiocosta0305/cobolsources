       IDENTIFICATION DIVISION.
       PROGRAM-ID.   TABLES00.
      *
      * ***************************************************
      * **************************************************
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WEEK-DAY-VALUES.
           05 FILLER PIC X(09) VALUE "SUNDAY   ".
           05 FILLER PIC X(09) VALUE "MONDAY   ".
           05 FILLER PIC X(09) VALUE "TUESDAY  ".
           05 FILLER PIC X(09) VALUE "WEDNESDAY".
           05 FILLER PIC X(09) VALUE "THURSDAY ".
           05 FILLER PIC X(09) VALUE "FRIDAY   ".
           05 FILLER PIC X(09) VALUE "SATUDAY  ".
       01 Weekday-Table REDEFINES WEEK-DAY-VALUES.
           05 WS-Day-of-Week PIC X(09) OCCURS 7 TIMES
              INDEXED BY WS-DW-ITER.
       01 WEEKEND-DAY-ITER   PIC 9 VALUE 0.
       01 WEEKEND-SEARCH     PIC X(09) VALUE SPACES.
       PROCEDURE DIVISION.
       000-TOP-LEVEL.
           PERFORM VARYING WEEKEND-DAY-ITER FROM 1 BY 1
              UNTIL WEEKEND-DAY-ITER > 7
                 DISPLAY WS-Day-of-Week (WEEKEND-DAY-ITER)
           END-PERFORM.
           PERFORM VARYING WS-DW-ITER FROM 1 BY 1
              UNTIL WS-DW-ITER > 7
                 DISPLAY WS-Day-of-Week (WS-DW-ITER)
           END-PERFORM.
           SET WS-DW-ITER TO 1.
           INITIALIZE WEEKEND-SEARCH.
           MOVE "FRIDAY" TO WEEKEND-SEARCH.
           SEARCH WS-Day-of-Week
              AT END DISPLAY "Day not found"
              WHEN WS-Day-of-Week (WS-DW-ITER) = WEEKEND-SEARCH
                 SET WEEKEND-DAY-ITER TO WS-DW-ITER
                 DISPLAY WEEKEND-DAY-ITER
           END-SEARCH.
           SET WS-DW-ITER TO 1.
           INITIALIZE WEEKEND-SEARCH.
           MOVE "SEXTA" TO WEEKEND-SEARCH. *> Friday on PT_BR
           SEARCH WS-Day-of-Week
              AT END DISPLAY "Day not found"
              WHEN WS-Day-of-Week (WS-DW-ITER) = WEEKEND-SEARCH
                 SET WEEKEND-DAY-ITER TO WS-DW-ITER
                 DISPLAY WEEKEND-DAY-ITER
           END-SEARCH.
           GOBACK.

