# Workshop 5

# 5.1 - CBL0006

After opening ACCT-REC (for reading) and PRINT-LINE (for writing) datasets and mounting a report header, it will start to read the entries from ACCT-REC and verify it is from Virginia. If so, it'll add 1 to VIRGINIA-CLIENTS. Then it will move the data from the entry (ACCT-NO, ACCT-LIMIT, ACCT-NO, ACCT-BALANCE and LAST-NAME) to the respective fields on PRINT-REC, and then WRITE the item. 

To finish, it'll print the number of clients from Virginia.