       IDENTIFICATION DIVISION.
       PROGRAM-ID. MIDTERM.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-DATA
              ASSIGN TO CLAIM
              ORGANIZATION IS SEQUENTIAL.
           SELECT OUTPUT-DATA
              ASSIGN TO CLAIMRPT
              ORGANIZATION IS SEQUENTIAL.
           SELECT WRONG-DATA
              ASSIGN TO CLAIMERR
              ORGANIZATION IS SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD INPUT-DATA
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 80 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS INPUT-REC.
       01 INPUT-REC.
           05 FILLER            PIC X(37).
           05 TEST-MONTH        PIC 9(2).
              88 MONTH-30         VALUE 4, 6, 9, 11.
              88 MONTH-31         VALUE 1, 3, 5, 7, 8, 10, 12.
              88 MONTH-28         VALUE 2.
           05 FILLER            PIC X(2).
           05 TEST-AMOUNT       PIC X(9).
           05 TEST-DEDUCTABLE   PIC X(4).
           05 TEST-COINSURANCE  PIC X(2).
           05 FILLER            PIC X(24).

       FD OUTPUT-DATA
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 132 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS OUTPUT-REC.
       01 OUTPUT-REC   PIC X(132).

       FD WRONG-DATA
           RECORDING MODE IS F
           LABEL RECORDS ARE STANDARD
           RECORD CONTAINS 132 CHARACTERS
           BLOCK CONTAINS 0 RECORDS
           DATA RECORD IS WRONG-REC.
       01 WRONG-REC    PIC X(132).

       WORKING-STORAGE SECTION.

       01 WRONG-DATA-WS.
           05 WRONG-MESSAGE      PIC X(48)      VALUE SPACES.
           05 WRONG-LINE-NUM     PIC 9(4)       VALUE ZEROES.
           05 WRONG-LINE         PIC X(80)      VALUE SPACES.

       01 CLAIM-VALUES-WS.
           05 CLAIM-AMOUNT-WS           PIC S9(7)V99   VALUE ZERO.
           05 CLAIM-AMOUNT-PAID-WS      PIC S9(7)V99   VALUE ZERO.
           05 POLICY-DEDUCTIBLE-PAID-WS PIC S9(4)      VALUE ZERO.
           05 COMPANY-PERCENT-WS        PIC V9(4)      VALUE .002.
           05 CLAIM-COPAY-MET           PIC X          VALUE "Y".

       01 FILE-FLAGS.
           05 INPUT-EOF         PIC X          VALUE "N".
              88 IS-INPUT-EOF                  VALUE "Y".
           05 WRONG-DATA-FLAG   PIC X          VALUE "N".
              88 IS-WRONG-DATA                 VALUE "Y".
       01 NUM-OF-RECS           PIC 9999       VALUE ZERO.
       01 DIVIDE-RESULT         PIC 9999       VALUE ZERO.
       01 PROCESS-CLAIM         PIC X          VALUE "Y".
           88 PROCESS-THIS-CLAIM               VALUE "Y".
           88 NOT-PROCESS-THIS-CLAIM           VALUE "N".
       01 LEAP-YEAR             PIC 9          VALUE ZERO.
           88 IS-LEAP-YEAR                     VALUE 0.

       01 TODAY-DATE.
           05 TODAY-YEAR        PIC 9999       VALUE 0.
           05 TODAY-MONTH       PIC 99         VALUE 0.
           05 TODAY-DAY         PIC 99         VALUE 0.
           05 FILLER            PIC X(12).


      * Data COPYBOOK
           COPY CLAIMREC.

      * Report COPYBOOK
           COPY CLAIMRPT.

       PROCEDURE DIVISION.

           PERFORM OPEN-FILES.
           PERFORM START-HOUSEKEEP.
           PERFORM PROCESS-DATA UNTIL IS-INPUT-EOF.
           PERFORM CLOSE-DATA.
           GOBACK.

       OPEN-FILES.
           OPEN INPUT INPUT-DATA.
           OPEN OUTPUT OUTPUT-DATA.
           OPEN OUTPUT WRONG-DATA.
      *     DISPLAY "OPEN DATA".

       START-HOUSEKEEP.
           MOVE FUNCTION CURRENT-DATE TO TODAY-DATE.
           MOVE TODAY-DAY TO CLAIM-DAY-HDR.
           MOVE TODAY-MONTH TO CLAIM-MONTH-HDR.
           MOVE TODAY-YEAR TO CLAIM-YEAR-HDR.
           PERFORM PRINT-HEADER.

       PRINT-HEADER.
           MOVE CLAIM-PAGE-HDR TO OUTPUT-REC.
           WRITE OUTPUT-REC AFTER ADVANCING 1.
           MOVE CLAIM-HDR-1 TO OUTPUT-REC.
           WRITE OUTPUT-REC.
           MOVE CLAIM-HDR-2 TO OUTPUT-REC.
           WRITE OUTPUT-REC.
           MOVE CLAIM-LINE-1 TO OUTPUT-REC.
           WRITE OUTPUT-REC.

       PROCESS-DATA.
           PERFORM HOUSEKEEP.
           PERFORM READ-DATA.
           IF NOT IS-INPUT-EOF
              ADD 1 TO NUM-OF-RECS
      *        DISPLAY NUM-OF-RECS
              PERFORM START-VALIDATE-ENTRY
                 THRU END-VALIDADE-ENTRY
              IF NOT IS-WRONG-DATA THEN
                 PERFORM CALCULATE-CLAIM
                 IF PROCESS-THIS-CLAIM THEN
                    PERFORM PRINT-REPORT
                 END-IF
              ELSE
                 PERFORM LOG-WRONG-DATA
              END-IF
           END-IF.

       CLOSE-DATA.
           CLOSE INPUT-DATA.
           CLOSE OUTPUT-DATA.
           CLOSE WRONG-DATA.
           MOVE ZERO TO NUM-OF-RECS.
      *     DISPLAY "CLOSE-DATA".

       HOUSEKEEP.
      *    DISPLAY "HOUSEKEEP".
           INITIALIZE WRONG-DATA-FLAG.
           INITIALIZE WRONG-DATA-WS.
           INITIALIZE CLAIM-RECORD-WS.
           INITIALIZE PROCESS-CLAIM.

       READ-DATA.
      *    DISPLAY "READING DATA".
           READ INPUT-DATA
              INTO CLAIM-RECORD-WS
              AT END MOVE "Y" TO INPUT-EOF
           END-READ.

      * Used only for testing purpose
      *     IF NOT IS-INPUT-EOF THEN
      *        MOVE "Y" TO WRONG-DATA-FLAG
      *     END-IF.
       CALCULATE-CLAIM.
      *     MOVE .0002 TO POLICY-COINSURANCE.
           COMPUTE POLICY-DEDUCTIBLE-PAID =
              POLICY-AMOUNT * COMPANY-PERCENT-WS.

           IF POLICY-DEDUCTIBLE-PAID > CLAIM-AMOUNT-PAID THEN
              COMPUTE CLAIM-AMOUNT-PAID =
                 CLAIM-AMOUNT-PAID -
                    (POLICY-COINSURANCE * CLAIM-AMOUNT)
              MOVE "N" TO CLAIM-COPAY-MET
           ELSE
              COMPUTE CLAIM-AMOUNT-PAID =
                 CLAIM-AMOUNT-PAID - POLICY-DEDUCTIBLE-PAID -
                    (POLICY-COINSURANCE * CLAIM-AMOUNT)
              MOVE "Y" TO CLAIM-COPAY-MET
           END-IF.

      *     DISPLAY INSURED-DETAILS.
      *     DISPLAY CLAIM-AMOUNT-PAID.

           IF (POLICY-AMOUNT - CLAIM-AMOUNT-PAID) > 0
           THEN
              MOVE "Y" TO PROCESS-CLAIM
           ELSE
              MOVE "N" TO PROCESS-CLAIM
           END-IF.

           DISPLAY PROCESS-CLAIM.
      *    DISPLAY "CALCULATE CLAIM".

       PRINT-REPORT.
           PERFORM FORMAT-REPORT.
           PERFORM WRITE-REPORT.

       FORMAT-REPORT.
           EVALUATE TRUE
              WHEN PRIVATE
                 MOVE 'EMPLOYER-PRIVATE' TO POLICY-TYPE-RPT
              WHEN MEDICARE
                 MOVE 'STANDARD MEDICARE' TO POLICY-TYPE-RPT
              WHEN AFFORDABLE-CARE
                 MOVE 'AFFORDABLE CARE ACT' TO POLICY-TYPE-RPT
           END-EVALUATE.
           MOVE INSURED-FIRST-NAME TO POLICY-FIRST-NAME-RPT.
           MOVE INSURED-LAST-NAME TO POLICY-LAST-NAME-RPT.
           MOVE INSURED-POLICY-NO TO POLICY-NUMBER-RPT.
           MOVE POLICY-YEAR TO POLICY-YEAR-RPT.
           MOVE POLICY-MONTH TO POLICY-MONTH-RPT.
           MOVE POLICY-DAY TO POLICY-DAY-RPT.
           MOVE CLAIM-COPAY-MET TO POLICY-COPAY-MET-RPT.
           MOVE .002 TO POLICY-COPAY-PCT-RPT.
           MOVE POLICY-DEDUCTIBLE-PAID TO POLICY-DEDUCT-PAID-RPT.
           MOVE CLAIM-AMOUNT TO POLICY-CLAIM-AMT-RPT.
           MOVE CLAIM-AMOUNT-PAID TO POLICY-CLAIM-PAID-RPT.

      *     DISPLAY "FORMAT REPORT".

       WRITE-REPORT.
           MOVE CLAIM-DATA-RPT TO OUTPUT-REC.
           WRITE OUTPUT-REC.
      *     DISPLAY "WRITE DATA".

       LOG-WRONG-DATA.
      *     DISPLAY "WRONG DATA".
      *     MOVE "WRONG ENTRY" TO WRONG-MESSAGE.
      *     MOVE 1 TO WRONG-LINE-NUM.
      *     MOVE CLAIM-RECORD-WS TO WRONG-LINE.
           MOVE WRONG-DATA-WS TO WRONG-REC.
           WRITE WRONG-REC.

      * Start to validating data
       START-VALIDATE-ENTRY.
      * 1- Missingn Data
      *     DISPLAY INSURED-POLICY-NO.
           IF INSURED-POLICY-NO = ZERO
              OR INSURED-POLICY-NO = LOW-VALUE
              OR INSURED-POLICY-NO = SPACES
              OR INSURED-POLICY-NO IS NOT NUMERIC
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing or invalid Insure Number"
                    TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 2- Missing name part
      *    DISPLAY INSURED-LAST-NAME.
           IF IS-EMPTY-FIRST-NAME OR IS-EMPTY-LAST-NAME THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing First or Last Name" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.
      *     IF INSURED-LAST-NAME <= SPACES THEN
      *           MOVE "Y" TO WRONG-DATA-FLAG
      *           MOVE "Missing Last Name" TO WRONG-MESSAGE
      *           MOVE NUM-OF-RECS TO WRONG-LINE-NUM
      *           GO TO END-VALIDADE-ENTRY
      *     END-IF.

      *     DISPLAY INSURED-FIRST-NAME.
      *     IF INSURED-FIRST-NAME <= SPACES THEN
      *           MOVE "Y" TO WRONG-DATA-FLAG
      *           MOVE "Missing First Name" TO WRONG-MESSAGE
      *           MOVE NUM-OF-RECS TO WRONG-LINE-NUM
      *           GO TO END-VALIDADE-ENTRY
      *     END-IF.

      * 3- Missing Policy Type
           IF NOT (PRIVATE OR MEDICARE OR AFFORDABLE-CARE)
              OR POLICY-TYPE IS NOT NUMERIC  THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing or Invalid Policy" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 4- Missing Date or Invalid Date
      *     DISPLAY POLICY-BENEFIT-DATE-NUM.
           IF POLICY-BENEFIT-DATE-NUM = ZERO
              OR POLICY-BENEFIT-DATE-NUM = SPACES
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing Policy Benefit Date" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 5- Missing or Invalid Year
      *     DISPLAY POLICY-BENEFIT-DATE-NUM.
           IF POLICY-YEAR = ZERO
              OR POLICY-YEAR = SPACES
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing Policy Year" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 6- Missing or Invalid Month
           IF POLICY-MONTH = ZERO
              OR POLICY-MONTH = SPACES
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing Policy Month" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

           IF NOT (POLICY-MONTH >= 1 AND POLICY-MONTH <= 12)
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 STRING "Invalid Policy Month - "
                    POLICY-MONTH DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 7- Missing or Invalid Day
           IF POLICY-DAY = ZERO
              OR POLICY-DAY = SPACES
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing Policy Day" TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 7a - Too big/small day
           IF NOT (POLICY-DAY >= 1 AND POLICY-DAY <= 31)
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 STRING "Invalid Policy Day - "
                    POLICY-DAY DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 7b - Over 30 day for a MONTH-30 day
           IF MONTH-30 AND (POLICY-DAY > 30)
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 STRING "Invalid Policy Day - "
                    POLICY-DAY DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 7c - Over 31 day for a MONTH-31 day
           IF MONTH-31 AND (POLICY-DAY > 31)
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 STRING "Invalid Policy Day - "
                    POLICY-DAY DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 7d - Over 28 day for a MONTH-28 day
           IF MONTH-28
              THEN
      * Finding leap year (just by divide by 4 & taking the remaining)
                 PERFORM VALIDATE-LEAP-YEAR
           END-IF.

      * 8- Missing or Invalid Amount
           DISPLAY TEST-AMOUNT.
           IF TEST-AMOUNT  = ZERO
              OR TEST-AMOUNT = SPACE
              OR TEST-AMOUNT IS NOT NUMERIC
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing or Invalid Policy Amount"
                    TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 9- Missing or Invalid Deductible
           IF TEST-DEDUCTABLE  = ZERO
              OR TEST-DEDUCTABLE = SPACE
              OR TEST-DEDUCTABLE IS NOT NUMERIC
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing or Invalid Policy Deductible Pay"
                    TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

      * 10- Missing or Invalid Coinsurance
           IF TEST-COINSURANCE = ZERO
              OR TEST-COINSURANCE = SPACE
              OR TEST-COINSURANCE IS NOT NUMERIC
              THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                 MOVE "Missing or Invalid Policy Co-Insurance Percent"
                    TO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
           END-IF.

       END-VALIDADE-ENTRY.
           IF IS-WRONG-DATA THEN
              MOVE CLAIM-RECORD-WS TO WRONG-LINE
      *        DISPLAY "Yes"
      *     ELSE
      *        DISPLAY "No"
           END-IF.

       VALIDATE-LEAP-YEAR.
           DIVIDE POLICY-YEAR BY 4
              GIVING DIVIDE-RESULT
              REMAINDER LEAP-YEAR.
      * Validating leap year
           IF IS-LEAP-YEAR THEN
              IF POLICY-DAY > 29 THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                    STRING "Invalid Policy Day - "
                       POLICY-DAY DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
              END-IF
           ELSE
              IF POLICY-DAY > 28 THEN
                 MOVE "Y" TO WRONG-DATA-FLAG
                    STRING "Invalid Policy Day - "
                       POLICY-DAY DELIMITED BY SIZE INTO WRONG-MESSAGE
                 MOVE NUM-OF-RECS TO WRONG-LINE-NUM
                 GO TO END-VALIDADE-ENTRY
              END-IF
           END-IF.
