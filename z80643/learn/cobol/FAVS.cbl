       IDENTIFICATION DIVISION.
       PROGRAM-ID. FAVS.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  FAV-REC.
           05  ARTIST-NAME            PIC X(30).
           05  NUMBER-OF-MUSICIANS    PIC 9(02).
           05  MUSICAL-GENRE          PIC X(12).
           05  COST.
              10  CD-COST             PIC 9(3)V99.
              10  SHIPPING-COST       PIC 9(2)V99.
              10  TAX                 PIC 9(2)V99.
           05 BAND-IS-STILL-TOGETHER  PIC X(1).
       01  TOTAL-COST                 PIC 9(3)V99.
       PROCEDURE DIVISION.
           MOVE "Pato Fu"       TO ARTIST-NAME.
           MOVE 4               TO NUMBER-OF-MUSICIANS.
           MOVE "Pop"           TO MUSICAL-GENRE.
           MOVE 0.03            TO TAX.
           MOVE 2.00            TO SHIPPING-COST.
           MOVE 4.00            TO CD-COST.
           MOVE "T"             TO BAND-IS-STILL-TOGETHER.
           COMPUTE TOTAL-COST =
                CD-COST + (CD-COST * TAX) + SHIPPING-COST.
           DISPLAY "Namr: " ARTIST-NAME.
           DISPLAY "Number of Musicians: " NUMBER-OF-MUSICIANS.
           DISPLAY "Genre: " MUSICAL-GENRE.
           DISPLAY "Still plays together? " BAND-IS-STILL-TOGETHER.
           DISPLAY "CD Cost: " SHIPPING-COST.
           DISPLAY "Taxes percent: " TAX.
           DISPLAY "Shipping: " SHIPPING-COST.
           DISPLAY "Total CD Cost: " TOTAL-COST.
           GOBACK.
