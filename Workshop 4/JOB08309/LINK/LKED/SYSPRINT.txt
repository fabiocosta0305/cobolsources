1z/OS V2 R3 BINDER     09:14:57 THURSDAY JUNE 25, 2020
 BATCH EMULATOR  JOB(Z51417G ) STEP(LKED    ) PGM= HEWL      PROCEDURE(LINK    )
 IEW2278I B352 INVOCATION PARAMETERS - MAP
 IEW2322I 1220  1        INCLUDE OBJ0000

1                         *** M O D U L E  M A P ***

 ---------------
 CLASS  B_TEXT            LENGTH =     1A50  ATTRIBUTES = CAT,   LOAD, RMODE=ANY
                          OFFSET =        0 IN SEGMENT 001     ALIGN = DBLWORD
 ---------------

  SECTION    CLASS                                      ------- SOURCE --------
   OFFSET   OFFSET  NAME                TYPE    LENGTH  DDNAME   SEQ  MEMBER

                 0  FAVRPT             CSECT       DF6  OBJ0000   01  FAVRPT

               DF8  CEESG005        *  CSECT        18  SYSLIB    01  CEESG005

               E10  CEEBETBL        *  CSECT        28  SYSLIB    01  CEEBETBL

               E38  CEESTART        *  CSECT        B0  SYSLIB    01  CEESTART

               EE8  IGZCBSO         *  CSECT       5A8  SYSLIB    01  IGZCBSO

              1490  CEEARLU         *  CSECT        B8  SYSLIB    01  CEEARLU

              1548  CEEBPIRA        *  CSECT       2A0  SYSLIB    01  CEEINT
        0     1548     CEEINT             LABEL
        0     1548     CEEBPIRB           LABEL
        0     1548     CEEBPIRC           LABEL

              17E8  CEECPYRT        *  CSECT        E2  SYSLIB    01  CEEINT

              18D0  CEEBPUBT        *  CSECT        70  SYSLIB    01  CEEBPUBT

              1940  CEEBTRM         *  CSECT        A4  SYSLIB    01  CEEBTRM

              19E8  CEEBLLST        *  CSECT        5C  SYSLIB    01  CEEBLLST
       10     19F8     CEELLIST           LABEL

              1A48  CEEBINT         *  CSECT         8  SYSLIB    01  CEEBINT
1          ***  DATA SET SUMMARY  ***

 DDNAME    CONCAT   FILE IDENTIFICATION

 OBJ0000     01     Z51417.COBOBJS.OBJ
 SYSLIB      01     CEE.SCEELKED
1          *** SYMBOL REFERENCES NOT ASSOCIATED WITH ANY ADCON ***

 TYPE   SCOPE   NAME


           *** E N D  O F  M O D U L E  M A P ***



                                 *** O P E R A T I O N   S U M M A R Y   R E P O R T ***

1PROCESSING OPTIONS:

    ALIASES             NO
    ALIGN2              NO
    AMODE               UNSPECIFIED
    CALL                YES
    CASE                UPPER
    COMPAT              UNSPECIFIED
    COMPRESS            AUTO
    DCBS                NO
    DYNAM               NO
    EXTATTR             UNSPECIFIED
    EXITS:              NONE
    FILL                NONE
    GID                 UNSPECIFIED
    HOBSET              NO
    INFO                NO
    LET                 04
    LINECT              060
    LIST                SUMMARY
    LISTPRIV            NO
    LONGPARM            NO
    MAP                 YES
    MAXBLK              032760
    MODMAP              NO
    MSGLEVEL            00
    OVLY                NO
    PRINT               YES
    RES                 NO
    REUSABILITY         UNSPECIFIED
    RMODE               UNSPECIFIED
    RMODEX              NO
    SIGN                NO
    STORENX             NOREPLACE
    STRIPCL             NO
    STRIPSEC            NO
    SYMTRACE
    TERM                NO
    TRAP                ON
    UID                 UNSPECIFIED
    UPCASE              NO
    WKSPACE             000000K,000000K
    XCAL                NO
    XREF                NO
    ***END OF OPTIONS***




1SAVE OPERATION SUMMARY:

    MEMBER NAME         FAVRPT
    LOAD LIBRARY        Z51417.LEARN.LOAD
    PROGRAM TYPE        PROGRAM OBJECT(FORMAT 2)
    VOLUME SERIAL       VPWRKA
    DISPOSITION         REPLACED
    TIME OF SAVE        09.14.58  JUN 25, 2020


1SAVE MODULE ATTRIBUTES:

    AC                  000
    AMODE                31
    COMPRESSION         NONE
    DC                  NO
    EDITABLE            YES
    EXCEEDS 16MB        NO
    EXECUTABLE          YES
    LONGPARM            NO
    MIGRATABLE          YES
    OL                  NO
    OVLY                NO
    PACK,PRIME          NO,NO
    PAGE ALIGN          NO
    REFR                NO
    RENT                NO
    REUS                NO
    RMODE               ANY
    SCTR                NO
    SIGN                NO
    SSI
    SYM GENERATED       NO
    TEST                NO
    XPLINK              NO
    MODULE SIZE (HEX)   00001A50
    DASD SIZE (HEX)     00006000


1 ENTRY POINT AND ALIAS SUMMARY:

  NAME:            ENTRY TYPE AMODE C_OFFSET CLASS NAME        STATUS

  FAVRPT            MAIN_EP      31 00000000 B_TEXT

                          *** E N D   O F   O P E R A T I O N   S U M M A R Y   R E P O R T ***




1z/OS V2 R3 BINDER     09:14:57 THURSDAY JUNE 25, 2020
 BATCH EMULATOR  JOB(Z51417G ) STEP(LKED    ) PGM= HEWL      PROCEDURE(LINK    )
 IEW2008I 0F03 PROCESSING COMPLETED.  RETURN CODE =  0.



1----------------------
 MESSAGE SUMMARY REPORT
 ----------------------
  TERMINAL MESSAGES      (SEVERITY = 16)
  NONE

  SEVERE MESSAGES        (SEVERITY = 12)
  NONE

  ERROR MESSAGES         (SEVERITY = 08)
  NONE

  WARNING MESSAGES       (SEVERITY = 04)
  NONE

  INFORMATIONAL MESSAGES (SEVERITY = 00)
  2008  2278  2322


  **** END OF MESSAGE SUMMARY REPORT ****

