       IDENTIFICATION DIVISION.
       PROGRAM-ID. SUB03.
       DATA DIVISION.
       LINKAGE SECTION.
       01  Z    PIC X(05).   *> Linkage Parms
       01  K    PIC X(05).   *> Values passed
       01  U    PIC X(05).   *> from SUB01
       PROCEDURE DIVISION USING Z, K, U.
      *> Addressability to data from SUB01
           DISPLAY Z, K, U
           MOVE '2' TO Z, K, U.
           DISPLAY Z, K, U
           GOBACK. *> Control returned to SUB01
