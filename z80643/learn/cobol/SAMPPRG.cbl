       IDENTIFICATION DIVISION.
       PROGRAM-ID. SAMPLE.
      * Comment: 7This program Displays a number of text strings
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 PRICE PIC 9(5)v99.
       77 EDITED-PRICE PIC $zz,zz9.99.
       01 VALS.
           05 ZEROVAL PIC 9 VALUE 0.
           05 VAL PIC 9(5)V99.
       PROCEDURE DIVISION.
           MOVE 12345.6 TO PRICE.
           DIVIDE PRICE BY ZEROVAL GIVING VAL
              ON SIZE ERROR DISPLAY 'XABLAU'.
           MOVE PRICE TO EDITED-PRICE.
           DISPLAY EDITED-PRICE
           GOBACK.
